/**
 * @author Victor Hugo V. Sousa
 * @mail vhv.sousa@gmail.com
 * @website bracode.net
 * @GitHub github.com/VictorHVS
 * @date 02/04/2014
 */
package br.ifpi.entity;

public class Participante {
	
	private	int 	id;
	private	String	cpf;
	private	String	nome;
	private	String	fone;
	private	String	perfil;
	public	static	int	contador;
	
	public Participante() {
		
	}
	
	public Participante(String cpf, String nome, String fone, String perfil) {
		
		this.nome	= nome;
		this.cpf 	= cpf;
		this.fone	= fone;
		this.perfil	= perfil;
		
	}
	
	public void generatedID(){
		this.id = contador;
		contador++;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getFone() {
		return fone;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	@Override
	public String toString(){
		return "ID: "+id+" - CPF: "+cpf+" - NOME: "+nome+" - FONE: "+fone+" - PERFIL: "+perfil;
	}

}
