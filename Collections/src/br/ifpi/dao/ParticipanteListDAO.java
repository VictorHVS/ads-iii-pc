/**
 * @author Victor Hugo V. Sousa
 * @mail vhv.sousa@gmail.com
 * @website bracode.net
 * @GitHub github.com/VictorHVS
 * @date 02/04/2014
 */
package br.ifpi.dao;

import java.util.ArrayList;
import java.util.List;

import br.ifpi.entity.Participante;

public class ParticipanteListDAO implements ParticipanteDAO {

	private List<Participante> participantes;

	public ParticipanteListDAO() {
		participantes = new ArrayList<>();
	}

	@Override
	public void save(Participante entity) {
		if (entity.getId() == 0) {
			entity.generatedID();
			participantes.add(entity);
		} else {
			/* Algum codigo aqui sem sentido aparente */
		}
	}

	@Override
	public void delete(int id) {
		for (Participante p : participantes) {
			if (p.getId() == id) {
				participantes.remove(p);
				break;
			}
		}
	}

	@Override
	public Participante find(int id) {
		for (Participante p : participantes) {
			if (p.getId() == id) {
				return p;
			}
		}
		return null;
	}

	@Override
	public List<Participante> find() {	

		return participantes;
	}

	@Override
	public Participante findByCpf(String cpf) {
		for (Participante p : this.participantes) {
			if (p.getCpf().equals(cpf)) {
				return p;
			}
		}
		return null;
	}

	@Override
	public List<Participante> findByNome(String nome) {
		List<Participante> p = new ArrayList<>();
		
		for (Participante participante : this.participantes) {
			if (participante.getNome().contains(nome)){
				p.add(participante);
			}
		}
		return p;
	}

}
