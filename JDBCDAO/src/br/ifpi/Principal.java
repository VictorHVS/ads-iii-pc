/**
 * @author Victor Hugo V. Sousa
 * @mail vhv.sousa@gmail.com
 * @website bracode.net
 * @GitHub github.com/VictorHVS
 * @date 03/04/2014
 */
package br.ifpi;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.ifpi.dao.ParticipanteDAO;
import br.ifpi.dao.ParticipanteJDBCDAO;
import br.ifpi.dao.ParticipanteListDAO;
import br.ifpi.entity.Participante;

public class Principal {

	public static void main(String[] args) {

		//ParticipanteDAO dao = new ParticipanteListDAO();
		ParticipanteDAO dao = new ParticipanteJDBCDAO();

		String op;
		String menu = "1. Inserir Participante\n2. Atualizar Cadastro\n3. Remover Participante\n4. Exibir Participante por CPF\n5. Exibir Participante por ID\n6. Pesquisa por nome\nx. Sair";
		
		do {
			op = JOptionPane.showInputDialog(menu, "x");
			switch (op) {
			//Cadastro
			case "1": {
				Participante participante = new Participante();
				
				participante.setNome(JOptionPane.showInputDialog("Digite seu Nome"));
				participante.setCpf(JOptionPane.showInputDialog("Digite seu CPF"));
				participante.setFone(JOptionPane.showInputDialog("Digite seu Fone"));
				participante.setPerfil(JOptionPane.showInputDialog("Digite seu Perfil"));
				
				dao.save(participante);
				break;
			}
			//Alteracao
			case "2":{
				Participante participante = new Participante();
				String cpf;
				cpf = JOptionPane.showInputDialog("Digite seu CPF");
				participante = dao.findByCpf(cpf);
				
				if(participante != null){
					participante.setNome(JOptionPane.showInputDialog("Insira o novo Nome", participante.getNome()));
					participante.setCpf(JOptionPane.showInputDialog("Insira o novo CPF", participante.getCpf()));
					participante.setFone(JOptionPane.showInputDialog("Insira o novo Fone", participante.getFone()));
					participante.setPerfil(JOptionPane.showInputDialog("Insira o novo Perfil", participante.getPerfil()));;
				}
				else{
					JOptionPane.showMessageDialog(null, "CPF n�o cadastrado");
				}
				break;
			}
			//Remover por cpf
			case "3":{
				Participante participante = new Participante();
				String cpf;
				cpf = JOptionPane.showInputDialog("Digite o CPF");
				participante = dao.findByCpf(cpf);
				
				if(participante != null){
					dao.delete(participante.getId());
				}
				else{
					JOptionPane.showMessageDialog(null, "CPF n�o cadastrado");
				}
				break;
			}
			//Ixibir por cpf
			case "4":{
				Participante participante = new Participante();
				String cpf;
				cpf = JOptionPane.showInputDialog("Digite seu CPF");
				participante = dao.findByCpf(cpf);
				
				if(participante != null){
					JOptionPane.showMessageDialog(null, participante.toString());
				}
				else{
					JOptionPane.showMessageDialog(null, "CPF n�o cadastrado");
				}
				break;
			}
			//Exibir por ID
			case "5":{
				Participante participante = new Participante();
				String id;
				id = JOptionPane.showInputDialog("Entre com o ID");
				participante = dao.find(Integer.parseInt(id));
				
				if(participante != null){
					JOptionPane.showMessageDialog(null, participante.toString());
				}
				else{
					JOptionPane.showMessageDialog(null, "CPF n�o cadastrado");
				}
				break;
			}
			//Pesquisa por nome
			case "6":{
				List<Participante> lista = new ArrayList<>();
				String nome;
				String listaDeNomes = "";
				nome = JOptionPane.showInputDialog("Entre com o nome: ");
				lista = dao.findByNome(nome);
				
				if(lista != null && lista.size() != 0){
					for(Participante p : lista){
						listaDeNomes += p.toString() + "\n";
					}
					JOptionPane.showMessageDialog(null, listaDeNomes);
				}
				else{
					JOptionPane.showMessageDialog(null, "Nome ou trecho n�o encontrado em nossa base de dados ultra secreta.");
				}
				break;
			}
			}
		} while (!op.equals("x"));
	}
}