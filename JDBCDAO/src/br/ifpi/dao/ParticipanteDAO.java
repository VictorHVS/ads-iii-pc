/**
 * @author Victor Hugo V. Sousa
 * @mail vhv.sousa@gmail.com
 * @website bracode.net
 * @GitHub github.com/VictorHVS
 * @date 02/04/2014
 */
package br.ifpi.dao;

import java.util.List;

import br.ifpi.entity.Participante;

public interface ParticipanteDAO {

	public void save(Participante entity);
	void delete(int id);
	Participante find(int id);
	List<Participante> find();
	Participante findByCpf(String cpf);
	List<Participante> findByNome(String str);

}
