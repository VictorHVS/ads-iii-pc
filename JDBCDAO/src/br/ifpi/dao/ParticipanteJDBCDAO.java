/**
 * @author Victor Hugo V. Sousa
 * @mail vhv.sousa@gmail.com
 * @website bracode.net
 * @GitHub github.com/VictorHVS
 * @date 12/05/2014
 */
package br.ifpi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Persistencia.JDBC.FabricaDeConexoes;
import br.ifpi.entity.Participante;

public class ParticipanteJDBCDAO implements ParticipanteDAO {

	private Connection con;

	public ParticipanteJDBCDAO() {
		try {
			this.con = FabricaDeConexoes.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void save(Participante entity) {
		try {
			String sql = "insert into participante(nome, cpf, fone, perfil) values(?, ?, ?, ?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, entity.getNome());
			ps.setString(2, entity.getCpf());
			ps.setString(3, entity.getFone());
			ps.setString(4, entity.getPerfil());

			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void delete(int id) {
		try {
			String sql = "Delete From participante where id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);

			ps.execute();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Participante find(int id) {
		String sql = "SELECT * FROM participante WHERE id = ?";
		Participante participante = null;
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeQuery();
			ResultSet rs = ps.getResultSet();
			if (rs.next()) {
				participante = new Participante();
				participante.setId(rs.getInt("id"));
				participante.setCpf(rs.getString("cpf"));
				participante.setNome(rs.getString("nome"));
				participante.setFone(rs.getString("fone"));
				participante.setPerfil(rs.getString("perfil"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return participante;
	}

	@Override
	public List<Participante> find() {
		String sql = "SELECT * FROM participante";
		List<Participante> participantes = null;
		Participante p = null;
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement(sql);
			ResultSet rs = ps.getResultSet();
			participantes = new ArrayList<Participante>();
			while(rs.next()){
				p = new Participante();
				p.setId(rs.getInt("id"));
				p.setCpf(rs.getString("cpf"));
				p.setNome(rs.getString("nome"));
				p.setFone(rs.getString("fone"));
				p.setPerfil(rs.getString("perfil"));
				participantes.add(p);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return participantes;
	}

	@Override
	public Participante findByCpf(String cpf) {
		String sql = "SELECT * FROM participante WHERE cpf = ?";
		Participante participante = null;
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, cpf);
			ps.execute();
			ResultSet rs = ps.getResultSet();
			if (rs.next()) {
				participante = new Participante();
				participante.setId(rs.getInt("id"));
				participante.setCpf(rs.getString("cpf"));
				participante.setNome(rs.getString("nome"));
				participante.setFone(rs.getString("fone"));
				participante.setPerfil(rs.getString("perfil"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return participante;
	}

	@Override
	public List<Participante> findByNome(String str) {
		String sql = "SELECT * FROM participante WHERE nome LIKE ?";
		List<Participante> participantes = null;
		Participante p = null;
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, "%" + str + "%");
			ResultSet rs = ps.executeQuery();
			participantes = new ArrayList<Participante>();
			while(rs.next()){
				p = new Participante();
				p.setId(rs.getInt("id"));
				p.setCpf(rs.getString("cpf"));
				p.setNome(rs.getString("nome"));
				p.setFone(rs.getString("fone"));
				p.setPerfil(rs.getString("perfil"));
				participantes.add(p);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return participantes;
	}

}
