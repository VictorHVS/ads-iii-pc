/**
 * @author Victor Hugo V. Sousa
 * @mail vhv.sousa@gmail.com
 * @website bracode.net
 * @GitHub github.com/VictorHVS
 * @Bitbucket bitbucket.org/VictorHVS
 * @date 22/05/2014
 */
package net.bracode.entity;

public class Enemy {
	
	private String name;
	private String type;

	public Enemy(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}
	
	public String getName(){
		return name;
	}

	@Override
	public String toString(){
		return "nome: "+this.name+" type: "+this.type;
	}

}
