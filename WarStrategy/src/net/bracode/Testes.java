/**
 * @author Victor Hugo V. Sousa
 * @mail vhv.sousa@gmail.com
 * @website bracode.net
 * @GitHub github.com/VictorHVS
 * @Bitbucket bitbucket.org/VictorHVS
 * @date 22/05/2014
 */
package net.bracode;

import net.bracode.entity.Enemy;
import net.bracode.strategy.War;

public class Testes {

	static Enemy eua = new Enemy("Nuclear", "EUA");
	static Enemy brasil = new Enemy("Fragil", "BRASIL");
	static Enemy inglaterra = new Enemy("GrandeExercito", "INGLATERRA");
	
	static War w = new War(eua);

	public static void main(String[] args) {
		
		Enemy m = new Enemy("Nuclear", "URSS");
		War g = new War(m);
		System.out.println(g.getStrategy());
		
		w.setEnemy(eua);
		inicia();
		finaliza();

		w.setEnemy(brasil);
		inicia();
		finaliza();
		
		w.setEnemy(inglaterra);
		inicia();
		finaliza();
	
	}

	public static void inicia() {
		System.out.println("=========================");
		System.out.println("[Iniciando estrategia]");
		w.getStrategy().start();
		System.out.println();
	}

	public static void finaliza() {
		System.out.println("[Finalizando estrategia]");
		w.getStrategy().finish();
		System.out.println("=========================");
	}
}