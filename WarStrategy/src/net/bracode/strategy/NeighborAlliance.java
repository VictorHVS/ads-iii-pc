/**
 * @author Victor Hugo V. Sousa
 * @mail vhv.sousa@gmail.com
 * @website bracode.net
 * @GitHub github.com/VictorHVS
 * @Bitbucket bitbucket.org/VictorHVS
 * @date 22/05/2014
 */
package net.bracode.strategy;

public class NeighborAlliance implements WarStrategy {

	@Override
	public void start() {
		attackNorth();
		neighborAttackSouth();
	}


	@Override
	public void finish() {
		divideBenefits();
		divideCosts();

	}

	private void divideCosts() {
		System.out.println("Dividir Custos de Reconstrução.");
		
	}

	private void divideBenefits() {
		System.out.println("Dividir Benefícios.");
		
	}
	
	private void attackNorth() {
		System.out.println("Atacar ao Norte.");
		
	}

	private void neighborAttackSouth() {
		System.out.println("Vizinho atacar ao Sul.");
		
	}
	
	@Override
	public  String toString(){
	return "Fazer Aliança com Vizinho!";
	}

}
