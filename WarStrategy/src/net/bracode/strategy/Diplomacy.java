/**
 * @author Victor Hugo V. Sousa
 * @mail vhv.sousa@gmail.com
 * @website bracode.net
 * @GitHub github.com/VictorHVS
 * @Bitbucket bitbucket.org/VictorHVS
 * @date 22/05/2014
 */
package net.bracode.strategy;

public class Diplomacy implements WarStrategy {

	@Override
	public void start() {
		retreatingTroops();
		proposeEconomicCooperation();
	}

	@Override
	public void finish() {
		disarmEnemy();
	}

	private void retreatingTroops() {
		System.out.println("Retirar Tropas.");

	}

	private void proposeEconomicCooperation() {
		System.out.println("Propor Acordo Economico.");

	}

	private void disarmEnemy() {
		System.out.println("Desarmar Inimigo.");

	}

	@Override
	public  String toString(){
	return "Diplomacia";
	}
}
