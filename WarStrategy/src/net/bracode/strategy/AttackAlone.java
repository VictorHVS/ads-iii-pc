/**
 * @author Victor Hugo V. Sousa
 * @mail vhv.sousa@gmail.com
 * @website bracode.net
 * @GitHub github.com/VictorHVS
 * @Bitbucket bitbucket.org/VictorHVS
 * @date 22/05/2014
 */
package net.bracode.strategy;

public class AttackAlone implements WarStrategy {

	@Override
	public void start() {
		plantingEvidence();
		bombard();
		tangoDownGovernment();

	}

	@Override
	public void finish() {
		establishGovernmentAllied();

	}
	
	private void plantingEvidence() {
		System.out.println("Plantar Evidencias.");
		
	}

	private void bombard() {
		System.out.println("Lan�ar Bombas.");
		
	}

	private void tangoDownGovernment() {
		System.out.println("Derrubar Governo.");
		
	}

	private void establishGovernmentAllied() {
		System.out.println("Estabelecer Governo Amigo.");
		
	}
	
	@Override
	public  String toString(){
	return "Atacar Sozinho.";
	}

}
