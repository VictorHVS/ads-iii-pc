/**
 * @author Victor Hugo V. Sousa
 * @mail vhv.sousa@gmail.com
 * @website bracode.net
 * @GitHub github.com/VictorHVS
 * @Bitbucket bitbucket.org/VictorHVS
 * @date 22/05/2014
 */
package net.bracode.strategy;

import net.bracode.entity.Enemy;

public class War {

	private WarStrategy strategy;
	private Enemy enemy;

	public War(Enemy enemy) {
		init(enemy);
	}

	public void init(Enemy enemy) {
		this.enemy = enemy;
		defineStrategy(this.enemy);
	}

	public WarStrategy getStrategy() {
		return strategy;
	}

	public Enemy getEnemy() {
		return enemy;
	}

	public void setEnemy(Enemy enemy) {
		init(enemy);
	}

	public void defineStrategy(Enemy enemy) {
		try {
			if (enemy.getType().equals("Nuclear")) {
				this.strategy = new Diplomacy();
			} else if (enemy.getType().equals("GrandeExercito")) {
				this.strategy = new NeighborAlliance();
			} else if (enemy.getType().equals("Fragil")) {
				this.strategy = new AttackAlone();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
