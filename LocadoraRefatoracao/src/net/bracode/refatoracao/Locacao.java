package net.bracode.refatoracao;
/**
 * @author Victor Hugo V. Sousa
 * @mail vhv.sousa@gmail.com
 * @website bracode.net
 * @GitHub github.com/VictorHVS
 * @Bitbucket bitbucket.org/VictorHVS
 * @date 04/06/2014
 */

public class Locacao {
	private Filme _filme;
	private int _diasAlugados;

	public Locacao(Filme filme, int diasAlugados) {
		_filme = filme;
		_diasAlugados = diasAlugados;
	}

	public int lerDiasAlugados() {
		return _diasAlugados;
	}

	public Filme lerFilme() {
		return _filme;
	}

}
